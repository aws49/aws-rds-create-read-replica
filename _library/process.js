/**
 * Process Module.
 *
 * Consists of useful functions and methods for
 * work with processes.
 */

// Global Variables:
let _tstart,
    _tasks
;

/**
 * Exports Module Functions.
 */
module.exports = {

    /**
     * The Process Exit.
     *
     * Exits from process.
     *
     * @param {Error} errMsg
     *      A message of Error.
     */
    exit: (errMsg=undefined) => {
        if (errMsg)
            console.error(errMsg);

        process.stdin.destroy();
        process.exit(0);
    },

    /**
     * The Task Manager.
     *
     * Executes functions in tasks.
     *
     * @param {any} result
     *      A parameter to the next function of tasks.
     */
    next: function (result=undefined) {
        if (!Array.isArray(_tasks) || !_tasks.length)
            this.exit(new Error('[!] There are NOT tasks to execute!'));

        const current_task = _tasks.shift();

        if (current_task)
            current_task(result);
    },

    /**
     * The Process Start.
     *
     * Saves the time of the start.
     * Starts the process of executing tasks (if tasks exist).
     *
     * @param {(function(...any[]))[]} tasks
     *      A array of functions to execute.
     */
    start: function (tasks) {
        _tstart = new Date().getTime();

        if (tasks)
        {
            _tasks = tasks;

            this.next();
        }
    },

    /**
     * The Process Time.
     *
     * Returns a time from between called function
     * process.start() and process.time().
     *
     * @return {string}
     *      A time [format: 00:00:00.000]
     */
    time: function () {

        if (!_tstart)
        {
            console.log(
                '[!] The function process.start() were NOT called.',
                'For using process.time() needs to call it in the begging of the program.'
            );
            this.exit();
        }

        let _tend = new Date().getTime();

        return App.lib.date_time.ms_to_time(_tend - _tstart);
    },

};

