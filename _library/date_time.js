/**
 * DateTime Module.
 *
 * Consists of useful functions and methods for
 * work with date and time.
 */

// Global Variables:
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

/**
 * Exports Module Functions.
 */
module.exports = {

    /**
     * Date Difference in Days.
     * Gives 2 dates objects (new Date()) and,
     * return a difference in days.
     *
     * @param {Object} date1
     *      A first date in seconds.
     * @param {Object} date2
     *      A second date in seconds.
     *
     * @return {integer}
     *      A number of difference days date1 from date2.
     */
    date_diff_in_days: (date1, date2) => Math.ceil( Math.abs(date2 - date1) / _MS_PER_DAY)
    ,

    /**
     * Convert milliseconds to time string (hh:mm:ss:mss).
     *
     * @param {Number} ms
     *      A time in milliseconds.
     *
     * @return {string}
     *      A time in the format: hh:mm:ss:mss.
     */
    ms_to_time: (ms) => new Date(ms).toISOString().slice(11, -1)
    ,

    /**
     * The Time of Time Zone.
     *
     * Gets the time of given time zone.
     *
     * @param {string} timezone
     *      A time zone.
     *
     * @return {string}
     *      A string of given time zone.
     */
    iso_timezone_datetime: (timezone) => new Date().toLocaleString('sv-SE', {
            timeZone: timezone
        }).replace(' ', 'T')
    ,

};


