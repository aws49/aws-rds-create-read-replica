/**
 * AWS Requests Module.
 *
 * Consists useful functions and methods for
 * making a request to AWS Services via AWS SDK.
 */

/**
 * NPM Modules
 *
 * Documentation:
 * @module aws-sdk - https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS.html
 */
const AWS = require('aws-sdk');

// AWS Configuration Variables:
AWS.config.loadFromPath(Config.aws_credentials_path);
AWS.config.apiVersions = {
    // AWS SDK service API versions
    s3: '2006-03-01',
    rds: '2014-10-31',
    ec2: '2016-11-15',
    elbv2: '2015-12-01',
    autoscaling: '2011-01-01'
};

// Global Variables:
const _rds = new AWS.RDS(),
      _s3 = new AWS.S3(),
      _ec2 = new AWS.EC2(),
      _elbv2 = new AWS.ELBv2();
      _autoscaling = new AWS.AutoScaling();

/**
 * Exports Module Functions.
 */
module.exports = {

    // AWS S3 Requests
    s3: {

        /**
         * AWS S3 List Buckets Request.
         * Requests to AWS S3 via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @return {Promise<ListBucketsOutput>}
         *      A data of buckets.
         */
        list_buckets: () => new Promise(resolve =>
            _s3.listBuckets((err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,

        /**
         * AWS S3 List Objects Request.
         * Requests to AWS S3 via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {ListObjectsRequest} params
         *      A parameters for ListObjectsRequest to AWS S3.
         *
         * @return {Promise<ListObjectsOutput>}
         *      A data of objects.
         */
        list_objects: (params) => new Promise(resolve =>
            _s3.listObjects(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,

        /**
         * AWS S3 Upload Object Request.
         * Requests to AWS S3 via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {{Bucket: string, Body: module:stream.internal.PassThrough, Key: string}} params
         *      A parameters for PutObjectRequest to AWS S3.
         * @param {ManagedUploadOptions} options
         *
         * @return {Promise<ManagedUpload.SendData>}
         *      A data of upload object.
         */
        upload_object: (params, options={}) => new Promise(resolve =>
            _s3.upload(params, options, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,

        /**
         * AWS S3 Delete Objects Request.
         * Requests to AWS S3 via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {DeleteObjectsRequest} params
         *      A parameters for DeleteObjectsRequest to AWS S3.
         *
         * @return {Promise<DeleteObjectsOutput>}
         *      A response of request to delete.
         */
        delete_objects: (params) => new Promise(resolve =>
            _s3.deleteObjects(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,

    },

    // AWS RDS Requests
    rds: {

        /**
         * AWS RDS Describe DB Instances Request.
         * Requests to AWS RDS via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {DescribeDBInstancesMessage} params
         *      A parameters for DescribeDBInstancesRequest to AWS RDS.
         *
         * @return {Promise<DBInstanceMessage>}
         *      A data of the db instances.
         */
        get_db_instances: (params = {}) => new Promise(resolve =>
            _rds.describeDBInstances(params, (err, data) =>
                resolve(_check_err_res(err, data.DBInstances))
            ))
        ,

        /**
         * AWS RDS Create DB Instance Request.
         * Requests to AWS RDS via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {CreateDBInstanceMessage} params
         *      A parameters for CreateDBInstanceRequest to AWS RDS.
         *
         * @return {Promise<CreateDBInstanceResult>}
         *      The data of a created db instance.
         */
        create_db_instance: (params) => new Promise(resolve =>
            _rds.createDBInstance(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,

        /**
         * AWS RDS Wait For Resource State.
         * Waits a resource state on the DB Instance.
         * Requests to AWS RDS via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {DescribeDBInstancesMessage} params
         *      The parameters for waitFor() AWS RDS Request.
         * @param {string} resourceState
         *      [required] The states of resource:
         *        dBInstanceAvailable (default),
         *        dBInstanceDeleted,
         *        dBSnapshotAvailable,
         *        dBSnapshotDeleted,
         *        dBClusterSnapshotAvailable,
         *        dBClusterSnapshotDeleted.
         *
         * @return {Promise<DBInstanceMessage>}
         *      The data of the db instance which wait for.
         */
        wait_for: (params, resourceState = 'dBInstanceAvailable') =>
            new Promise(resolve => _rds.waitFor(resourceState = 'dBInstanceAvailable', params, (
                err,
                data
                ) => resolve(_check_err_res(err, data.DBInstances))
            ))
        ,

        /**
         * AWS RDS Create Read Replica Request.
         * Requests to AWS RDS via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {CreateDBInstanceReadReplicaMessage} params
         *      The parameters for createDBInstanceReadReplica()
         *      AWS RDS Request.
         *
         * @return {Promise<CreateDBInstanceReadReplicaResult>}
         *      The data of the new read replica instance.
         */
        create_read_replica: (params) => new Promise(resolve =>
            _rds.createDBInstanceReadReplica(params, (err, data) =>
                resolve(_check_err_res(err, data.DBInstance))
            ))
        ,

    },

    // AWS EC2 Requests:
    ec2: {

        /**
         * AWS EC2 Describe Instances Request.
         * Requests to AWS EC2 via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {DescribeInstancesRequest} params
         *      The parameters for describeInstances()
         *      AWS EC2 Request.
         *
         * @return {Promise<DescribeInstancesResult>}
         *      The data of instances.
         */
        describe_instances: (params) => new Promise(resolve =>
            _ec2.describeInstances(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,

        /**
         * AWS EC2 Describe Network Interfaces Request.
         * Requests to AWS EC2 via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {DescribeNetworkInterfacesRequest} params
         *      The parameters for describeNetworkInterfaces()
         *      AWS EC2 Request.
         *
         * @return {Promise<DescribeNetworkInterfacesResult>}
         *      The data of network interfaces.
         */
        describe_network_interfaces: (params) => new Promise(resolve =>
            _ec2.describeNetworkInterfaces(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,

        /**
         * AWS EC2 Describe Security Groups.
         * Requests to AWS EC2 via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {DescribeSecurityGroupsRequest} params
         *      The parameters for describeSecurityGroups()
         *      AWS EC2 Request.
         *
         * @return {Promise<DescribeSecurityGroupsResult>}
         *      The data of security groups.
         */
        describe_security_groups: (params) => new Promise(resolve =>
            _ec2.describeSecurityGroups(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,
    },

    // AWS ELB v2 Requests:
    elbv2: {
        /**
         * AWS ELB Describe Load Balancers Request.
         * Requests to AWS ELB via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {DescribeLoadBalancersInput} params
         *      The parameters for describeLoadBalancers()
         *      AWS ELB Request.
         *
         * @return {Promise<DescribeLoadBalancersOutput>}
         *      The data of load balancers.
         */
        describe_load_balancers: (params) => new Promise(resolve =>
            _elbv2.describeLoadBalancers(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,
    },

    // AWS EC2 Auto Scaling Requests:
    autoscaling: {
        /**
         * AWS EC2 Describe Auto Scaling Groups Request.
         * Requests to EC2 Auto Scaling via SDK and,
         * if successful, it will return data of response.
         * Otherwise, throw the exception.
         *
         * @param {AutoScalingGroupNamesType} params
         *      The parameters for describeAutoScalingGroups()
         *      AWS EC2 Auto Scaling Request.
         *
         * @return {Promise<AutoScalingGroupsType>}
         *      The data of Auto Scaling Group.
         *
         */
        describe_auto_scaling_groups: (params) => new Promise(resolve =>
            _autoscaling.describeAutoScalingGroups(params, (err, data) =>
                resolve(_check_err_res(err, data))
            ))
        ,
    }

};

// Private Functions:

/**
 * Check Error AWS SDK Response.
 * If exist AWSError, it will throw
 * the exception with log.
 * Otherwise, return data.
 *
 * @param {Error} err
 *       AWSError
 * @param {any} data
 *       data of AWS Response
 *
 * @return data
 */
let _check_err_res = (err, data) => {
    if (err)
        throw err;
    return data;
};
