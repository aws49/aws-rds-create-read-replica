/**
 * _rds_create_read_replica.js
 *
 * This script gives the automated process of creating Read Replica
 * DB Instance in AWS RDS.
 * Creates Read Replica DB Instance from a source DB Instance (Master).
 *
 * Before using this script, sure that you have full-access
 * permissions to AWS RDS.
 *
 * @version - 1.0.0
 * @author  - Oleksii Pavliuk pavliuk.aleksey@gmail.com
 * @date    - 2020-07-28
 *
 */

// Turn ON strict mode
"use strict";

// A common global configurations:
global.Config = require('./config/_config');

// An app configurations:
global.App = {
    base_dir: __dirname,

    // Load NPM Modules:
    lib: require('./_library/_autoload'),
};

/*
 * Create Read Replica DB Instance.
 *
 * Create Read Replica DB Instance from another
 * DB Instance (Master) in AWS RDS via AWS SDK.
 */
// Global Variables:
const _createNewBD = 'Create A New DB Instance';

let _sourceDbInstance, _readReplicaInstance, _listOfDbInstances;

const tasks = [
    // Step 1: Getting a list of existing DB Instances from AWS RDS...
    () => {

        console.log('\nThe process of creating a Read Replica from DB Instance is started.');
        console.log('Getting the list of existing DB Instances...');

        App.lib.aws_rds.list_db_instances().then(listOfDbInstances => {
            // Add item createNewDB to array of keys:
            listOfDbInstances.push(_createNewBD);

            _listOfDbInstances = listOfDbInstances;

            App.lib.process.next();
        })
        .catch(errMsg => App.lib.process.exit(errMsg));
    },

    // Step 2: Selecting a source DB Instance from the list...
    () => {
        App.lib.user_requests.select_key('\nSelect a DB Instance to create a read replica from!',
            _listOfDbInstances
        )
        .then(dbInstanceName => {
            // Delete item createNewDB from array of keys:
            _listOfDbInstances.splice(_listOfDbInstances.indexOf(_createNewBD), 1);

            App.lib.process.next(dbInstanceName);
        });
    },

    // Step 3: Checking a picked solution of creating a new source DB Instance...
    (dbInstanceName) => {
        if (dbInstanceName === _createNewBD)
        {
            // Process of creating a New BD Instance:
            console.log('[!] Okay, you selected to create the NEW source BD Instance...');

            App.lib.aws_rds.create_db_instance().then(dbInstance => {
                _sourceDbInstance = dbInstance;

                App.lib.process.next();
            })
            .catch(errMsg => App.lib.process.exit(errMsg));
        }
        else
            App.lib.aws_requests.rds.get_db_instances({DBInstanceIdentifier: dbInstanceName})
            .then(dbInstances => {
                _sourceDbInstance = dbInstances[0];

                App.lib.process.next();
            })
            .catch(errMsg => App.lib.process.exit(errMsg));
    },

    // Step 4: Confirmation a picked source DB Instance...
    () => {
        App.lib.user_requests.confirm_solution(
            `A source BD Instance - ${_sourceDbInstance.DBInstanceIdentifier}`
        )
        .then(response => {
            if (response === false)
                throw new Error('The solution was NOT confirmed. The process was stopped.');

            App.lib.process.next();
        })
        .catch(errMsg => App.lib.process.exit(errMsg));
    },

    // Step 5: Checking a name for a NEW Read Replica DB Instance in the list...
    () => {
        const readReplicaName = `${_sourceDbInstance.DBInstanceIdentifier}-replica`;

        if (_listOfDbInstances.includes(readReplicaName))
        {
            console.log(
                `[!] The default Name [${readReplicaName}] for a Read Replica DB Instance has ALREADY EXISTED`,
                'in the list of DB Instances. You should input another an unique Name!'
            );

            App.lib.user_requests.user_input(
                'Please, give an unique name for a new Read Replica DB Instance: '
            )
            .then(userInput => App.lib.process.next(userInput));
        }
        else
            App.lib.process.next(readReplicaName);
    },

    // Step 6: Creating a NEW Read Replica DB Instance...
    (readReplicaName) => {
        console.log(`\n[!] The New Read Replica will have a DBInstanceIdentifier (Name): ${readReplicaName}`);

        App.lib.aws_rds.create_read_replica(_sourceDbInstance, {
            DBInstanceIdentifier: readReplicaName
        })
        .then((readReplicaInstance) => {
            _readReplicaInstance = readReplicaInstance;

            App.lib.process.next();
        })
        .catch(errMsg => App.lib.process.exit(errMsg));
    },

    // Step 7: Showing information about a new created Read Replica DB Instance...
    () => {
        let process_time = App.lib.process.time();

        console.log(_readReplicaInstance);
        console.log(
            `[!] A Read Replica DB Instance [${_readReplicaInstance.DBInstanceIdentifier}] was created successfully!`
        );
        console.log(`The process time: ${process_time}`);
        console.log('Done.');

        App.lib.process.exit();
    }
];

// Catch for uncaught exceptions:
process.on("uncaughtException", (err) => App.lib.process.exit(err));

// Starting the process of creating a Read Replica DB Instance...
App.lib.process.start(tasks);
