# AWS RDS Create Read Replica

## About

**AWS RDS Create Read Replica DB Instance**

**Script:** [`_rds_create_read_replica.js`](_rds_create_read_replica.js)  
**AWS SDK:** [` for JavaScript in Node.js`](https://aws.amazon.com/sdk-for-node-js/)  
**Language:** `JavaScript in Node.js`  

This script gives the automated process of creating a Read Replica DB Instance from 
an existing DB Instance in AWS RDS.  

**Author:** `Oleksii Pavliuk` _(pavliuk.aleksey@gmail.com)_  
**Date:** `2020-07-28`  

## Before using scripts:
- make sure you have full permissions to **AWS RDS**;
- [install AWS SDK and Dependencies](https://aws.amazon.com/developers/getting-started/nodejs/).

## Features

There are some scripts features which you need to know!  

The script does:
- asks to pick a source DB Instance from the list of an existing DB Instances;
- creates a New source DB Instance, if you pick this solution;
- asks to confirm a some important solutions and tasks;
- waits for availability of a source DB Instance to continue the creation process;
- waits for availability of a Read Replica DB Instance after the creation process.  

`[!]` The script creates a Read Replica ONLY from a source DB Instance which has an available state.
      It will be waiting for the availability of a source DB Instance to continue the process of creating.

